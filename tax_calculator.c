/* 
 * To ease your tiresome yearly tax calculation, this was created with love by :
 * Md. Nafiul Alam Fuji, SSE, REVE Systems - 2024
 * Enhanced and beautified by chatGPT <3
 */

#include <stdio.h>

#define ANSI_COLOR_BLACK   "\e[0;30m"
#define ANSI_COLOR_RED     "\e[0;31m"
#define ANSI_COLOR_GREEN   "\e[0;32m"
#define ANSI_COLOR_YELLOW  "\e[0;33m"
#define ANSI_COLOR_BLUE    "\e[0;34m"
#define ANSI_COLOR_MAGENTA "\e[0;35m"
#define ANSI_COLOR_CYAN    "\e[0;36m"
#define ANSI_COLOR_WHITE   "\e[0;37m"
#define ANSI_COLOR_RESET   "\e[0m"

#define ANSI_COLOR_HUBLACK   "\e[4;90m"
#define ANSI_COLOR_HURED     "\e[4;91m"
#define ANSI_COLOR_HUGREEN   "\e[4;92m"
#define ANSI_COLOR_HUYELLOW  "\e[4;93m"
#define ANSI_COLOR_HUBLUE    "\e[4;94m"
#define ANSI_COLOR_HUMAGENTA "\e[4;95m"
#define ANSI_COLOR_HUCYAN    "\e[4;96m"
#define ANSI_COLOR_HUWHITE   "\e[4;97m"


int main() {
    double salary1, salary2;
    printf(ANSI_COLOR_CYAN"Enter monthly salary for July-December, 2023 (ex: 90000) : "ANSI_COLOR_RESET);
    scanf("%lf", &salary1);
    printf(ANSI_COLOR_CYAN"Enter monthly salary for January-June, 2024 (ex: 100000) : "ANSI_COLOR_RESET);
    scanf("%lf", &salary2);

    // Net Salary Calculation
    double netSalary1 = salary1 * 0.965 * 6; // July-Dec
    double netSalary2 = salary2 * 0.965 * 6; // Jan-June
    double netSalary = netSalary1 + netSalary2;
    
    // Basic salary
    double basicSalary = ( (salary1 + salary2) / 4.0 ) * 12;

    // House rent
    double houseRent = basicSalary  * 0.5 ;

    // Convence allowance
    double convenceAllowance = 2500 * 12;

    // Medical allowance
    double medicalAllowance = basicSalary * 0.1;

    // Bonus Calculation
    double bonus = (salary2 / 2) + (salary2 / 2); // Bonus for both Eids

    // Provident Fund Calculation
    double providentFund1 = salary1 * 0.035 * 6; // Your contribution (July-Dec)
    double providentFund2 = salary2 * 0.035 * 6; // Your contribution (Jan-June)
    double totalYourContribution = providentFund1 + providentFund2;
    double companyContribution = totalYourContribution;
    double totalProvidentFund = totalYourContribution + companyContribution;

    // Total Income Calculation
    double totalIncome = netSalary + bonus + totalProvidentFund;

    // Other Allowances
    double otherAllowances = totalIncome - (basicSalary + bonus + houseRent + convenceAllowance + medicalAllowance + companyContribution);

    // Taxable Income Calculation
    double deduction = totalIncome / 3 < 450000 ? totalIncome / 3 : 450000;
    double taxableIncome = totalIncome - deduction;

    // Tax Calculation
    double tax = 0;
    double remaining1 = taxableIncome;
    double tax1 = (remaining1 > 350000) ? 350000 * 0 : remaining1 * 0;
    remaining1 -= ( (remaining1 > 350000) ? 350000 : remaining1);
    
    double tax2 = (remaining1 > 100000) ? 100000 * 0.05 : remaining1 * 0.05;
    double remaining2 = remaining1 - ( (remaining1 > 100000) ? 100000 : remaining1);

    double tax3 = (remaining2 > 400000) ? 400000 * 0.10 : remaining2 * 0.10;
    double remaining3 = remaining2 - ( (remaining2 > 400000) ? 400000 : remaining2);

    double tax4 = (remaining3 > 500000) ? 500000 * 0.15 : remaining3 * 0.15;
    double remaining4 = remaining3 - ((remaining3 > 500000) ? 500000 : remaining3);
    
    double tax5 = (remaining4 > 500000) ? 500000 * 0.2 : remaining4 * 0.2;
    double remaining5 = remaining4 - ((remaining4 > 400000) ? 400000 : remaining4);
    
    double tax6 = remaining5 * 0.25;

    tax = tax1 + tax2 + tax3 + tax4 + tax5 + tax6;

    // Rebate Calculation
    double rebateLimit = 1000000;
    double rebate = taxableIncome * 0.03;
    rebate = (rebate > rebateLimit) ? rebateLimit : rebate ;
    rebate = (rebate > tax) ? tax : rebate;
    double requiredInvestment = rebate / 0.15;
    double netRequiredInvestment = ( requiredInvestment > totalProvidentFund ) > 0 ? ( requiredInvestment - totalProvidentFund ) : 0 ;
    double rebateByMyInvestment, rebateByProvidentFund;
    if(netRequiredInvestment == 0){
        rebateByMyInvestment = 0;
    }else{
        rebateByMyInvestment = netRequiredInvestment * 0.15;
    }
    rebateByProvidentFund = totalProvidentFund * 0.15;
    double taxAfterRebate = tax - rebate;
    double monthlyTaxCharge = taxAfterRebate/12.0;

    // Output the Report
    printf("\n"ANSI_COLOR_GREEN"################################################################"ANSI_COLOR_RESET"\n");
    printf(ANSI_COLOR_GREEN"                   Tax Report 2023-24 (REVE)"ANSI_COLOR_RESET);
    printf("\n"ANSI_COLOR_GREEN"################################################################"ANSI_COLOR_RESET"\n\n");

    printf("------------------------------------------------------------------------------------------------\n");
    printf("| report is generated considering :                                                            |\n");
    printf("|  - you have same salary from july,2023 to dec,2023 and from jan,2024 to june,2024            |\n");
    printf("|  - you got 2 festival bonus in 2024 within jan-june                                          |\n");
    printf("|  - provident fund is 3.5%% of your salary and company is contributing same 3.5%% to            |\n"); 
    printf("|    that (company contribution requires atleast 2 years of employment)                        |\n");
    printf("------------------------------------------------------------------------------------------------\n\n");

    printf(ANSI_COLOR_CYAN"**********************\n| Income Calculation |\n**********************"ANSI_COLOR_RESET"\n\n");
    printf("\t Total Income = (i) Net Salary + (ii) Festival Bonus + (iii) Provident Fund\n\n");
    printf("\t (i) Net Salary = %.2lf * 96.5%% * 6 (July-Dec, 2023)\n", salary1);
    printf("\t                  + %.2lf * 96.5%% * 6 (Jan-June, 2024)\n", salary2);
    printf("\t                = %.2lf + %.2lf\n", netSalary1, netSalary2);
    printf("\t                = %.2lf/- \n", netSalary);
    printf("\t (ii) Festival Bonus = %.2lf / 2 (Eid ul Fitr, 2024) + %.2lf / 2 (Eid ul Azha, 2024)\n", salary2, salary2);
    printf("\t            = %.2lf/- \n", bonus);
    printf("\t (iii) Provident Fund = My Contribution + Company Contribution\n");
    printf("\t                      = %.2lf * 3.5%% * 6 (July-Dec, 2023)\n", salary1);
    printf("\t                        + %.2lf * 3.5%% * 6 (Jan-June, 2024)\n", salary2);
    printf("\t                        + Company Contribution\n");
    printf("\t                      = %.2lf + %.2lf + Company Contribution \n", providentFund1, providentFund2); 
    printf("\t                      = %.2lf + Company Contribution \n", totalYourContribution);    
    printf("\t                      = %.2lf + %.2lf (As, Company Contribution = Your Contribution)\n", totalYourContribution, companyContribution);
    printf("\t                      = %.2lf/- \n\n", totalProvidentFund);
    printf(ANSI_COLOR_GREEN"\t Total Income (2023-24 year) = (i) %.2lf + (ii) %.2lf + (ii) %.2lf\n", netSalary, bonus, totalProvidentFund);
    printf("\t                             = %.2lf/-"ANSI_COLOR_RESET"\n\n", totalIncome);

    printf("\t Basic Income = Net Salary + My Contribution to Provident Fund \n");
    printf("\t              = %.2lf + %.2lf\n", netSalary, totalYourContribution);
    printf("\t              = %.2lf/-\n\n", basicSalary);

    printf("\t House Rent = Basic Salary * 50%%\n");
    printf("\t            = %.2lf * 50%%\n", basicSalary);
    printf("\t            = %.2lf/-\n\n", houseRent);

    printf("\t Conveyance Allowance = 2500 * 12\n");
    printf("\t                     = %.2lf/-\n\n", convenceAllowance);

    printf("\t Medical Allowance = Basic Salary * 10%%\n");
    printf("\t                  = %.2lf * 10%%\n", basicSalary);
    printf("\t                  = %.2lf/-\n\n", medicalAllowance);

    printf("\t Other Allowances = Total Income - \n");
    printf("\t (Basic Salary + Bonus + House Rent + Provident Fund + Conveyance + Medical Allowance)\n");
    printf("\t                  = %.2lf - (%.2lf + %.2lf + %.2lf + %.2lf + %.2lf)\n", totalIncome, basicSalary, bonus, houseRent, convenceAllowance, medicalAllowance);
    printf("\t                  = %.2lf/-\n\n", otherAllowances);

    printf(ANSI_COLOR_CYAN"*******************\n| Tax Calculation |\n*******************"ANSI_COLOR_RESET"\n\n");
    printf(ANSI_COLOR_RED"\t ## Taxable Income is after the deduction of the lower \n"ANSI_COLOR_RESET);
    printf(ANSI_COLOR_RED"\t between ( 1/3 of Total Income or 4,50,000/- ) from Total Income\n\n"ANSI_COLOR_RESET);
    printf("\t 1/3 of Total Income = %.2lf / 3\n", totalIncome);
    printf("\t                     = %.2lf/- \n", totalIncome / 3);
    (450000 != deduction) ? printf("\t As 1/3 is lower than 450000/-,"):printf("\t As 450000/- is lower than 1/3,");
    printf("\t deduction = %.2lf/- \n\n", deduction);
    printf("\t Total Taxable Income (2023-24 year) = %.2lf - %.2lf\n", totalIncome, deduction);
    printf("\t                                     = %.2lf/-\n\n", taxableIncome);


    printf(ANSI_COLOR_YELLOW"\t +------------------------+-------------+-------------+\n");
    printf("\t | Tax Bracket            | Tax Amount  | Remaining   |\n");
    printf("\t +------------------------+-------------+-------------+\n");
    printf("\t |                        |             | %11.2lf |\n", taxableIncome);
    printf("\t | 0%%  for 1st  3,50,000  | %11.2lf | %11.2lf |\n", tax1, remaining1);
    printf("\t | 5%%  for next 1,00,000  | %11.2lf | %11.2lf |\n", tax2, remaining2);
    printf("\t | 10%% for next 4,00,000  | %11.2lf | %11.2lf |\n", tax3, remaining3);
    printf("\t | 15%% for next 5,00,000  | %11.2lf | %11.2lf |\n", tax4, remaining4);
    printf("\t | 20%% for next 5,00,000  | %11.2lf | %11.2lf |\n", tax5, remaining5);
    printf("\t | 25%% for rest of amount | %11.2lf | %11.2lf |\n", tax6, 0);
    printf("\t +------------------------+-------------+-------------+\n");
    printf("\t |     Total Tax          | %11.2lf/-             |\n", tax);
    printf("\t +------------------------+-------------+-------------+"ANSI_COLOR_RESET"\n\n");

    printf(ANSI_COLOR_GREEN"\t So, your initial tax before rebate is %.2lf/- "ANSI_COLOR_RESET"\n\n", tax);


    printf(ANSI_COLOR_CYAN"**********************\n| Rebate Calculation |\n**********************"ANSI_COLOR_RESET"\n\n");
    printf(ANSI_COLOR_RED"\t ## Eligible Tax Rebate is up to 3%% of your taxable income \n\t ## Rebate amount is 15%% of your investment \n\t ## Rebate highest limit is 10,00,000/- \n\n"ANSI_COLOR_RESET);
    printf("\t Eligible tax rebate = 3%% of taxable income = %.2lf * 3%%\n", taxableIncome);
    printf("\t                     = %.2lf \n\n", rebate);
    printf("\t But to get this rebate You have to invest X amount of money where,\n");
    printf("\t X * 15%% = %.2lf\n", rebate);
    printf("\t or,   X = %.2lf / 15%%\n", rebate);
    printf("\t or,   X = %.2lf\n", requiredInvestment);
    printf("\t So, You need to invest at least %.2lf to get the highest tax rebate benefit available. \n\t More investment than this amount won't be accountable for rebate.\n\n", requiredInvestment);
    printf("\t Now, you already have a provident fund of %.2lf which is a part of the investment.\n", totalProvidentFund);
    if(requiredInvestment > totalProvidentFund){
        printf("\t So, Required Investment = %.2lf - %.2lf\n", requiredInvestment, totalProvidentFund);
        printf("\t                         = %.2lf/- and it can rebate %.2lf/-\n\n", netRequiredInvestment, rebateByMyInvestment);
        printf("\t Your Total Provident Fund is %.lf/- and it rebates %.lf/- automatically \n", totalProvidentFund, rebateByProvidentFund);
        printf("\t Eligible Rebate = %.2lf = %.2lf (by provident fund) + %.2lf (by investment) \n\n", rebate, rebateByProvidentFund, rebateByMyInvestment);
    }else{
        printf("\t as required investment %.2lf < total provident fund %.2lf, you dont need to invest\n\n", requiredInvestment, totalProvidentFund);
    }
    printf(ANSI_COLOR_GREEN"\t New tax after rebate (provided that you have invested accordingly) :"ANSI_COLOR_RESET"\n", taxAfterRebate);
    printf(ANSI_COLOR_GREEN"\t\t (Tax) %.2lf - (Rebate) %.2lf"ANSI_COLOR_RESET"\n", tax, rebate);
    printf(ANSI_COLOR_GREEN"\t\t = %.2lf"ANSI_COLOR_RESET"\n", taxAfterRebate);

    printf("\n"ANSI_COLOR_CYAN"******************\n| Final Overview |\n******************"ANSI_COLOR_RESET"\n\n");
    printf("\t ** So, you have a minimum tax liability in 2023-24 financial year is : \n");
    printf("\t    ---------------\n");
    printf("\t   | %11.2lf/- |\n", taxAfterRebate);
    printf("\t    ---------------\n");
    printf("\t    provided that you have invested at least %.2lf/- in any government certified \n", netRequiredInvestment);
    printf("\t    fund/charity/stock/other (eligible for tax rebate)\n\n");
    printf("\t ** And your monthly tax deduction from salary should be - \n");
    printf("\t    total yearly tax after rebate / 12 months : \n");
    printf("\t    ---------------\n");
    printf("\t   | %11.2lf/- |\n", monthlyTaxCharge);
    printf("\t    ---------------\n\n");

    if(netRequiredInvestment > 0) {
        printf(ANSI_COLOR_GREEN"\t **  So if you don't invest minimum required amount of %.2lf/- ,"ANSI_COLOR_RESET"\n",netRequiredInvestment); 
        printf(ANSI_COLOR_GREEN"\t you will (have to) give free extra %.2lf/- to government as tax for no reason ..!!\n\n"ANSI_COLOR_RESET"\n", rebateByMyInvestment);
    }
    printf("\t Salary Certificate\n");
    printf("\t ------------------\n");
    printf(ANSI_COLOR_YELLOW"\t +---------------------+--------------+\n");
    printf("\t |     Particulars     | Amount (Tk)  |\n");
    printf("\t +---------------------+--------------+\n");
    printf("\t | Basic Salary           %11.2lf |\n", basicSalary);
    printf("\t | Festival Bonus         %11.2lf |\n", bonus);
    printf("\t | House Rent             %11.2lf |\n", houseRent);
    printf("\t | Conveyance             %11.2lf |\n", convenceAllowance);
    printf("\t | Medical Allowance      %11.2lf |\n", medicalAllowance);
    printf("\t | PF (Company Part)      %11.2lf |\n", companyContribution);
    printf("\t | Other Allowance        %11.2lf |\n", otherAllowances);
    printf("\t +---------------------+--------------+\n");
    printf("\t |       Total         |  %11.2lf |\n", totalIncome);
    printf("\t +---------------------+--------------+"ANSI_COLOR_RESET"\n\n");


    getchar(); // Consume the newline character left by scanf
    getchar(); // Wait for user input

    return 0;
}