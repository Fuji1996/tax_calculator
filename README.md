** *Tax calculation here considers the 2023-24 tax year with respective tax rules in Bangladesh & specially modified for REVE employees* <br>
** *Build and Run environment is in Windows OS and you might require GCC to modify the c code and compile*

<h2>Example</h2>
<div style="display: flex; gap: 10px;">
  <img src="examples/sample_part1.png" alt="Example Image 1" width="90%">
  <img src="examples/sample_part2.png" alt="Example Image 2" width="90%">
  <img src="examples/sample_part3.png" alt="Example Image 3" width="90%">
</div>

<h2>How to run :</h2>

 - Double click the `tax_calculator.exe` in "_build_" folder and input your respective salary to get the tax calculation.

<h2>Build command :</h2> 
 
 - `gcc -o build/tax_calculator tax_calculator.c`

<h2>Input :</h2>
 
 - here, in reve many have increment cycle in january and many have in july
 - but tax year is from july to next years june
 - thats why here, the app takes input of monthly salary from july-december 
        and from january-june. if your salary is same from july to next years june
        you can simply inputs same amount for both input. otherwise just input respectively. 

<h2>Basic salary :</h2>

- Basic salary is 50% of total salary (with self contribution to provident fund & without company contribution to provident fund)

<h2>Festival bonus :</h2>

- here festival bonus is considered as 1 month salary of january-june cycle. you can change on your own

<h2>House Rent :</h2>

- 50% of basic salary

<h2>Conveyance :</h2>

- conveyance is considered as fixed 2500/- per month, as 30000/- for total year

<h2>Medical Allowance :</h2>

- 10% of basic salary

<h2>Provident fund :</h2>

 - here this considers 3.5% deduction as employee contribution to provident fund
        and later add same amount as company contribution (as per reve rules for employment more than 2 years)

 - and the whole provident fund amount is considered as an investment
